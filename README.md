Lite Sounds
================

Lite Sounds - to manage system wide sounds including login, logout and audible clicks.

## Screenshot:

![](https://imgur.com/K7WPMk5.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://gitlab.com/linuxlite/)
